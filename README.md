# debian10-perl528-apt

Plain debian buster (10) with perl 5.28 (as is). And a significant bundle of CPAN modules.

The existing apt "lib*-perl" Debian packages form a consistent development and run-time environment. They cover the vast majority of what I usually need. So apt can be a real alternative to cpanm (or similar). with pros and cons, as usual. Other projects that really need to use cpanm (or similar) can build on this one. Enjoy!

# etc, var, ...
later...

# left-out
- libdevel-cover-perl, libdevel-checklib-perl: they drag along gcc and cpp !!!
- libdbix-class-schema-loader-perl: drags along fontconfig and some fonts !!!
- libdbd-mysql-perl: maybe later
- libdbd-pg-perl: maybe later

# Cheers

Joao Miguel Ferreira

20200509

reviewed 20201027

P(orto)MT

;)
