FROM debian:buster

COPY README.md /force_rebuild_1598365942.txt

MAINTAINER João Miguel Ferreira <joao.miguel.c.ferreira@gmail.com>

ENV DEBIAN_FRONTEND=noninteractive

#debian buster is stable/frozen, except for security updates: let's get them.
RUN apt update
RUN apt upgrade -y
RUN apt install -y perl

#
#main generic libs
COPY apt-list-a.txt /build/
RUN apt install -y $(cat /build/apt-list-a.txt)
COPY apt-list-b.txt /build/
RUN apt install -y $(cat /build/apt-list-b.txt)

COPY . /app
WORKDIR /app

CMD ["prove"]
